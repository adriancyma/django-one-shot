from django.shortcuts import render, get_list_or_404, redirect
from todos.models import TodoList, TodoItem

# Create your views here.


def todos_list_list(request):
    lists = TodoList.objects.all()
    context = {"lists": lists}
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    list = TodoList.objects.get(id=id)
    # todo_list = get_object_or_404(TodoList, id=id)
    context = {
        "list": list,
    }
    return render(request, "todos/detail.html", context)


def todo_item_update(request, id):
    item = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItem(request.POST, instance=item)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItem(instance=item)
    context = {
        "form": form,
    }
    return render(request, "todo_items/update.html", context)
