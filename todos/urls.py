from django.urls import path
from todos.views import todos_list_list, todo_list_detail, todo_item_update

urlpatterns = [
    path("", todos_list_list, name="todos_list_list"),
    path("<int:id>/", todo_list_detail, name="todo_list_detail"),
    path("items/<int:id>/edit/", todo_item_update, name="todo_item_update"),
]
